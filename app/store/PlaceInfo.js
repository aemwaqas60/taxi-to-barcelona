const sequelize = require('../../dbConnection/mysql');
const Sequelize = require('sequelize');

const PlaceInfo = sequelize.define('PlaceInfo', {
        id: {
            type: Sequelize.UUID,
            primaryKey: true,
            autoIncrement: false,
            allowNull: false,
            defaultValue: Sequelize.UUIDV1,
        },
        palceId: {
            type: Sequelize.INTEGER
        },
        info: {
            type: Sequelize.STRING
        },
    },
    {
        freezeTableName: true,
    });
