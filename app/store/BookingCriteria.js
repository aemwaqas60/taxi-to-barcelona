const sequelize = require('../../dbConnection/mysql');
const Sequelize = require('sequelize');
const BookingCriteriaEntity = require('../entities/BookingCriteria');

const BookingCriteria = sequelize.define('BookingCriteria', {
        bookingCriteriaId: {
            type: Sequelize.UUID,
            primaryKey: true,
            autoIncrement: false,
            allowNull: false,
        },
        pickUpAddress: {
            type: Sequelize.STRING
        },
        pickUpId: {
            type: Sequelize.STRING
        },
        pickUpLng: {
            type: Sequelize.FLOAT
        },
        pickUpLat: {
            type: Sequelize.FLOAT
        },
        dropOffAddress: {
            type: Sequelize.STRING
        },
        dropOffId: {
            type: Sequelize.STRING
        },
        dropOffLng: {
            type: Sequelize.FLOAT
        },
        dropOffLat: {
            type: Sequelize.FLOAT
        },
        pickupTime: {
            type: Sequelize.DATE
        },
        passengerTotal: {
            type: Sequelize.INTEGER
        },
        casesTotal: {
            type: Sequelize.INTEGER
        },
        passengerName: {
            type: Sequelize.STRING,
        },
        cost: {
            type: Sequelize.INTEGER,
        },
        carType: {
            type: Sequelize.STRING,
        },
        wheelChair:{
            type: Sequelize.BOOLEAN,
        },
        flightNumber: {
            type: Sequelize.STRING,
        },
        bookingNote: {
            type: Sequelize.STRING,
        },
        termsOfService:{
            type: Sequelize.BOOLEAN,
        },
    },
    {
        freezeTableName: true,
    });

class BookingCriteriaStore {

    static async add(data) {
        try {
            const result = await BookingCriteria.create(data);
            return BookingCriteriaEntity.createFromObject(result);
        } catch (err) {
            throw err;
        }
    }

    static async find(bookingCriteriaId) {
        try {
            const result = await BookingCriteria.findOne({where: {bookingCriteriaId: bookingCriteriaId}});
            return BookingCriteriaEntity.createFromObject(result);
        } catch (err) {
            throw err;
        }
    }

    static async update(data) {
        try {
            await BookingCriteria.update(data, {where: {bookingCriteriaId: data.bookingCriteriaId}});
            return true;
        } catch (err) {
            throw err;
        }
    }
}

module.exports = BookingCriteriaStore;