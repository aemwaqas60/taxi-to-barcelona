const sequelize = require('../../dbConnection/mysql');
const BookingFactory = require('../entities/BookingFactory');
const PaymentStore = require('../store/Payment');
const DriverStore = require('../store/Driver');
const UserStore = require('../store/User');
const Sequelize = require('sequelize');

const BookingModel = sequelize.define('Booking', {
        bookingId: {
            type: Sequelize.UUID,
            primaryKey: true,
            autoIncrement: false,
            allowNull: false,
        },
        passengerName: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        pickupName: {
            type: Sequelize.STRING
        },
        pickupAddress: {
            type: Sequelize.STRING
        },
        pickupLng: {
            type: Sequelize.FLOAT
        },
        pickupLat: {
            type: Sequelize.FLOAT
        },
        dropoffName: {
            type: Sequelize.STRING
        },
        dropoffAddress: {
            type: Sequelize.STRING
        },
        dropoffLng: {
            type: Sequelize.FLOAT
        },
        dropoffLat: {
            type: Sequelize.FLOAT
        },
        wheelchair: {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
        },
        flightNumber: {
            type: Sequelize.STRING
        },
        cost: {
            type: Sequelize.INTEGER
        },
        distanceFee: {
            type: Sequelize.INTEGER
        },
        pickupTime: {
            type: Sequelize.DATE
        },
        passengerTotal: {
            type: Sequelize.INTEGER
        },
        casesTotal: {
            type: Sequelize.INTEGER
        },
        carType: {
            type: Sequelize.STRING
        },
        notes: {
            type: Sequelize.STRING
        },
        bookingAuth: {
            type: Sequelize.STRING
        },
        bookingRef: {
            type: Sequelize.STRING
        },
        bookingStatus: {
            type: Sequelize.STRING
        },
        paymentStatus: {
            type: Sequelize.STRING
        },
        paymentId: {
            type: Sequelize.STRING
        },
        userId: {
            type: Sequelize.UUID
        },
        driverId: {
            type: Sequelize.UUID
        },
    },
    {
        freezeTableName: true,
    });


class BookingStore {
    /**
     *
     * @param {Booking} booking
     * @returns {Promise<Booking>}
     */
    static async add(booking) {
        const result = await BookingModel.create(booking.toDBObject());
        return BookingStore.createBookingFromDbObject(result);
    }

    static async update(booking) {
        try {
            await BookingModel.update(booking.toDBObject(), {where: {bookingId: booking.bookingId}});
            return true;
        } catch (err) {
            console.log(err);
            return err;
        }
    }

    static async findById(bookingId) {
        const result = await BookingModel.findOne({where: {bookingId: bookingId}});
        return await BookingStore.createBookingFromDbObject(result);
    }

    static async findByUserId(userId, pageNumber) {
        try {
            const booking = {};
            const limit = 10;
            let offset = 0;
            const totalBookings = await BookingModel.findAndCountAll({where: {userId: userId}});
            let page = pageNumber;
            let pages = Math.ceil(totalBookings.count / limit);

            offset = limit * (page - 1);
            const paginatedBookings = await BookingModel.findAll({where: {userId: userId},
                limit: limit,
                offset: offset,
                $sort: {id: 1}
            });
            booking.bookings = await Promise.all(paginatedBookings.map(async booking => {
                return await BookingStore.createBookingFromDbObject(booking);
            }));
            booking.pages = pages;
            return booking;

        } catch (err) {
            console.log(err);
            return err;
        }
    }

    static async updateDriver(booking) {
        try {
            await BookingModel.update(booking, {where: {bookingId: data.bookingId}});
            return true;
        } catch (err) {
            console.log(err);
            return err;
        }
    }

    /**
     *
     * @param bookingObj
     * @returns {Promise<Booking>}
     */
    static async createBookingFromDbObject(bookingObj){
        const payment = await PaymentStore.findByBookingId(bookingObj.bookingId);
        const driver = await DriverStore.findById(bookingObj.driverId);
        const user = await UserStore.find(bookingObj);

        return BookingFactory.createFromDataBase(bookingObj, payment, driver, user);
    };
}

module.exports = BookingStore;
