const sequelize = require('../../dbConnection/mysql');
const Sequelize = require('sequelize');
const CarEntity = require('../entities/Car');

const Car = sequelize.define('Car', {
        id: {
            type: Sequelize.UUID,
            primaryKey: true,
            autoIncrement: false,
            allowNull: false,
            defaultValue: Sequelize.UUIDV1,
        },
        model: {
            type: Sequelize.STRING,
        },
        type: {
            type: Sequelize.STRING,
        },
        make: {
            type: Sequelize.INTEGER,
        },
        seats: {
            type: Sequelize.INTEGER,
        },
        bags: {
            type: Sequelize.INTEGER,
        },
        carNumber: {
            type: Sequelize.STRING,
        },
        driverId: {
            type: Sequelize.UUID,
        },
    },
    {
        freezeTableName: true,
    });


class CarStore {
    static async add(data) {
        try {
            const result = await Car.create(data);
            return CarEntity.createFromObject(result);
        } catch (err) {
            throw err;
        }
    }

    static async find(driverId) {
        try {
            const result = await Car.findOne({where: {driverId: driverId}});
            return result ? CarEntity.createFromObject(result) : null;
        } catch (err) {
            throw err;
        }
    }
}

module.exports = CarStore;
