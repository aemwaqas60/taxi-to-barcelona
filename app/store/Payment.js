const sequelize = require('../../dbConnection/mysql');
const Sequelize = require('sequelize');
const PaymentEntity = require('../entities/Payment');

const Payment = sequelize.define('Payment', {
        id: {
            type: Sequelize.UUID,
            primaryKey: true,
            autoIncrement: false,
            allowNull: false,
            defaultValue: Sequelize.UUIDV1,
        },
        transactionId: {
            type: Sequelize.STRING,
        },
        bookingId: {
            type: Sequelize.UUID,
        },
        chargedAmount: {
            type: Sequelize.FLOAT,
        },
        refunded: {
            type: Sequelize.BOOLEAN,
        },
    },
    {
        freezeTableName: true,
    });

class PaymentStore {

    static async add(data) {
        try {
            const result = await Payment.create(data);
            return PaymentEntity.createFromObject(result);
        } catch (err) {
            console.log(err);
            return err;
        }
    }

    static async findByBookingId(bookingId) {
        try {
            const result = await Payment.findOne({where: {bookingId: bookingId}});
            return PaymentEntity.createFromObject(result);
        } catch (err) {
            console.log(err);
            return err;
        }
    }

    static async update(payment) {
        try {
            await Payment.update(payment, {where: {id: payment.id}});
        } catch  (err) {
            return err;
        }

        return true;
    }
}

module.exports = PaymentStore;