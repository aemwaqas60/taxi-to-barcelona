const sequelize = require('../../dbConnection/mysql');
const Sequelize = require('sequelize');
const PricingTableEntitiy = require('../entities/PricingTable');

const PricingTable = sequelize.define('PricingTable', {
        id: {
            type: Sequelize.UUID,
            primaryKey: true,
            autoIncrement: false,
            allowNull: false,
            defaultValue: Sequelize.UUIDV1,
        },
        type: {
            type: Sequelize.STRING
        },
        pricePerKm: {
            type: Sequelize.FLOAT
        },
        priceMin: {
            type: Sequelize.FLOAT
        },
        carTypeCode: {
            type: Sequelize.STRING
        },
    },
    {
        freezeTableName: true,
    });

class PricingTableStore {
    static async find(carTypeCode, timeType) {
        try {
            const result = await PricingTable.findOne({where: {carTypeCode: carTypeCode, type: timeType}});
            return PricingTableEntitiy.createFromObject(result);
        } catch (e) {
            return false;
        }
    }
}

module.exports = PricingTableStore;