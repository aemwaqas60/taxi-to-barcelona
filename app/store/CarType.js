const sequelize = require('../../dbConnection/mysql');
const Sequelize = require('sequelize');
const CarTypeEntity = require('../entities/CarType');

const CarType = sequelize.define('CarType', {
        id: {
            type: Sequelize.UUID,
            primaryKey: true,
            autoIncrement: false,
            allowNull: false,
            defaultValue: Sequelize.UUIDV1,
        },
        name: {
            type: Sequelize.STRING
        },
        code: {
            type: Sequelize.STRING
        },
    },
    {
        freezeTableName: true,
    });



class CarTypeStore {
    static async findAll() {
        try {
            const carTypes = await CarType.findAll();
            return await Promise.all(carTypes.map(async carType => {
                return await CarTypeEntity.createFromObject(carType);
            }));
        } catch (err) {
            throw err;
        }
    }
}

module.exports = CarTypeStore;