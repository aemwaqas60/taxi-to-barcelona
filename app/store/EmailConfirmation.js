const sequelize = require('../../dbConnection/mysql');
const Sequelize = require('sequelize');
const EmailConfirmationEntity = require('../entities/EmailConfirmation');

const EmailConfirmation = sequelize.define('EmailConfirmation', {
        id: {
            type: Sequelize.UUID,
            primaryKey: true,
            autoIncrement: false,
            allowNull: false,
            defaultValue: Sequelize.UUIDV1,
        },
        confirmationCode: {
            type: Sequelize.STRING
        },
        confirmed: {
            type: Sequelize.BOOLEAN
        },
        userId: {
            type: Sequelize.UUID
        }
    },
    {
        freezeTableName: true,
    });

class EmailConfirmationStore {

    static async add(data) {
        try {
            const result = await EmailConfirmation.create(data);
            return EmailConfirmationEntity.createFromObject(result);
        } catch (e) {
            console.log(e);
            return err;
        }
    }

    static async updateStatus(confirmationCode, status = false) {
        try {
            const result = await EmailConfirmation.findOne({where: {confirmationCode: confirmationCode}});
            await result.update({confirmed: status});
            return EmailConfirmationEntity.createFromObject(result);
        } catch (e) {
            console.log(e);
            return err;
        }
    }

}

module.exports = EmailConfirmationStore;