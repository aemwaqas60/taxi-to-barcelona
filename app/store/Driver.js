const sequelize = require('../../dbConnection/mysql');
const DriverEntity = require('../entities/Driver');
const CarStore = require('./Car');
const Sequelize = require('sequelize');

const Driver = sequelize.define('Driver', {
        driverId: {
            type: Sequelize.UUID,
            primaryKey: true,
            autoIncrement: false,
            allowNull: false,
            defaultValue: Sequelize.UUIDV1,
        },
        name: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.STRING
        },
        idCard: {
            type: Sequelize.STRING
        },
        licenseNumber: {
            type: Sequelize.STRING
        },
        insuranceNumber: {
            type: Sequelize.STRING
        },
        countryCode: {
            type: Sequelize.INTEGER
        },
        phoneNumber: {
            type: Sequelize.INTEGER
        },
        photo: {
            type: Sequelize.STRING
        },
    },
    {
        freezeTableName: true,
    });

class DriverStore {
    static async add(data) {
        try {
            const result = await Driver.create(data);
            return DriverStore.createDriverFromDbObject(result);
        } catch (err) {
            throw err;
        }
    }

    static async findById(driverId) {
        try {
            const result = await Driver.findOne({where: {driverId: driverId}});
            return result ? DriverStore.createDriverFromDbObject(result) : null;
        } catch (err) {
            throw err;
        }
    }

    static async createDriverFromDbObject(driverObj) {
        try {
            const car = await CarStore.find(driverObj.driverId);

            return DriverEntity.createFromDataBase(driverObj, car);
        } catch (err) {
            throw err;
        }
    }
}

module.exports = DriverStore;