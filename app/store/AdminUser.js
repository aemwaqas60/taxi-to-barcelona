const AdminUserEntity = require('../entities/AdminUser');
const sequelize = require('../../dbConnection/mysql');
const Sequelize = require('sequelize');

const AdminUser = sequelize.define('AdminUser', {
        adminUserId: {
            type: Sequelize.UUID,
            primaryKey: true,
            autoIncrement: false,
            allowNull: false,
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true,
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        salt: {
            type: Sequelize.STRING,
            allowNull: false,
        },

    },
    {
        freezeTableName: true,
    });

class AdminUserStore {
    static async add(clientAdminUserObj) {
        try {
            const result = await AdminUser.create(clientAdminUserObj);
            return AdminUserEntity.createFromObject(result);
        } catch (err) {
            throw err;
        }
    }

    static async findByEmail(adminUser) {
        try {
            const result = await AdminUser.findOne({where: {email: adminUser.email}});
            return AdminUserEntity.createFromObject(result);
        } catch (err) {
            throw err;
        }
    }

    static async findById(adminUserId) {
        try {
            const result = await AdminUser.findOne({where: {adminUserId: adminUserId}});
            return AdminUserEntity.createFromObject(result);
        } catch (err) {
            throw err;
        }
    }
}

module.exports = AdminUserStore;

