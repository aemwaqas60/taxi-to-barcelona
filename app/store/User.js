const Sequelize = require('sequelize');
const sequelize = require('../../dbConnection/mysql');
const UserEntity = require('../entities/User');

const User = sequelize.define('User', {
    name: {type: Sequelize.STRING},
    userId: {type: Sequelize.UUID, primaryKey: true, autoIncrement: false, allowNull: false},
    email: {type: Sequelize.STRING},
    password: {type: Sequelize.STRING},
    salt: {type: Sequelize.STRING},
    phoneNumber: {type: Sequelize.INTEGER},
    countryCode: {type: Sequelize.INTEGER},
    loginType: {type: Sequelize.STRING},
}, {
    freezeTableName: true,
});


class UserStore {
    static async add(data) {
        try {
            const result = await User.create(data);
            return UserEntity.createFromObject(result);
        } catch (err) {
            throw err;
        }
    }

    static async find(data) {
        try {
            const result = await User.findOne({where: {userId: data.userId}});
            return UserEntity.createFromObject(result);
        } catch (err) {
            throw err;
        }
    }

    static async findByEmail(user) {
        try {
            const result = await User.findOne({where: {email: user.email}});
            return UserEntity.createFromObject(result);
        } catch (err) {
            throw err;
        }
    }

    static async findByPhoneNumber(user) {
        try {
            const result = await User.findOne({
                where: {
                    countryCode: user.phoneNumber.slice(1, 3),
                    phoneNumber: user.phoneNumber.slice(3)
                }
            });
            return UserEntity.createFromObject(result);
        } catch (err) {
            throw err;
        }
    }


    static async update(user) {
        try {
            const result = await User.update(user, {where: {userId: user.userId}});
            return UserEntity.createFromObject(result);
        } catch (err) {
            throw err;
        }
    }
}

module.exports = UserStore;
