const CarStore = require('../store/Car');
const CarEntity = require('../entities/Car');

class CarService {
    static async createCar(car){
        try {
            const resultObj = CarEntity.createCar(car.model, car.type, car.make, car.seats, car.bags, car.carNumber, car.driverId)
            await CarStore.add(resultObj);

            return true;
        } catch (err) {
            throw err;
        }
    }
}

module.exports = CarService;
