const PaymentStore = require('../store/Payment');
const PaymentEntity = require('../entities/Payment');
const Stripe = require('./Stripe');
const moment = require('moment');
const uuidv1 = require('uuid/v1');


class PaymentService {

    static async determineAmountToRefund(booking) {
        try {
            const now = moment(new Date);
            const pickUp = moment(booking.pickupTime);
            const remainingHours =  pickUp.diff(now, 'hours');

            const refundInLast24hours = 10; // percentage
            const refundBeforeLast24hours = 100; // percentage
            const charges = await Stripe.findTransaction(booking.payment.transactionId);
            const percentageRefund = (remainingHours <= 24) ? refundInLast24hours : refundBeforeLast24hours;
            return percentageRefund/100*(charges.amount/100);
        } catch(err) {
            return err;
        }
    }

    static async addPayment(user, booking, stripeToken){
        try {
            const charges = await Stripe.captureCharges(user, booking, stripeToken);
            const payment = PaymentEntity.createPayment(uuidv1(), charges.id, booking.bookingId,  booking.cost, false);
            return await PaymentStore.add(payment);
        } catch (err) {
            console.log(err);
            return err;
        }
    }

    static async refundPayment(booking){
        try {
            const refundAmount = await PaymentService.determineAmountToRefund(booking);

            await Stripe.refundCharge(booking.payment.transactionId, parseFloat(refundAmount));

            const payment = PaymentEntity.createFromObject(booking.payment);
            payment.setRefundStatus(true);

            await PaymentStore.update(payment);

            return refundAmount;
        } catch (err) {
            return err;
        }
    }
}

module.exports = PaymentService;