const CarTypeStore = require('../store/CarType');

class CarTypeService {
    static async getAllCarType(){
        try {
            return await CarTypeStore.findAll();
        } catch (err) {
            throw err;
        }
    }
}

module.exports = CarTypeService;
