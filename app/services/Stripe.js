const config = require('../../config/config');
const stripe = require("stripe")(config.stripe.secret);


class Stripe {

    static async captureCharges(user, booking, stripeToken){
        let transactionId = '';
        let chargeAmount = '';
        try {
            const charge = await stripe.charges.create({
                amount: parseFloat(booking.cost) * 100,         // convert euro to cents
                currency: "eur",
                source: stripeToken, // obtained with Stripe.js
                capture: false,
                metadata: {
                    booking_id: booking.bookingId,
                    user_id: user.userId,
                    name: user.name,
                },
            });

            transactionId = charge.id;
            chargeAmount = charge.amount;
            await stripe.charges.capture(charge.id);

            return charge;
        } catch(err) {
            console.log(err);
            await stripe.refunds.create(transactionId, chargeAmount);
            return err;
        }
    }

    static async findTransaction(transactionId){
        return await stripe.charges.retrieve(transactionId);
    }

    static async refundCharge(transactionId, amount){
        try {
            await stripe.refunds.create({ charge: transactionId, amount: amount * 100});
            return true;
        } catch(err) {
            console.log('Unable to refund charges with this id ', transactionId, err);
            return err;
        }
    }
}

module.exports = Stripe;