const UserStore = require('../store/User');
const User = require('../entities/User');
const passwordHandler = require('../../controllers/auth/verifyPassword');
const auth = require('../../controllers/auth/authMiddleware');
const EmailConfirmation = require('../store/EmailConfirmation');
const EmailConfirmationService = require('../services/UserEmailConfirmation');
const Notification = require('./Notification');


class UserService {
    static async createNewUser(data){
        try {

            const clientUserObj = await User.createUser(data.userId, data.name, data.email, data.password, null, data.phoneNumber, data.countryCode, data.loginType);
            const user = await UserStore.add(clientUserObj);

            const emailConfirmation = await EmailConfirmationService.createConfirmation(user);
            user.setEmailConfirmationCode(emailConfirmation);

            const jwtToken = await auth.generateToken(user);
            user.setJwtToken(jwtToken);

            await Notification.userSignUp(user.toObject());
            return user;
        } catch (err) {
            console.log(err);
            throw err;
        }
    }

    static async login(data){
        try {
            const user = await UserStore.findByPhoneNumber(data);
            await passwordHandler.verifyPassword(data.password, user);

            const jwtToken = await auth.generateToken(user);
            user.setJwtToken(jwtToken);

            return user;
        } catch (err) {
            console.log(err);
            throw err;
        }
    }

    static async forgetPassword(data){
        try {
            const user = await UserStore.findByPhoneNumber(data);
            await Notification.forgetPassword(user);
            return true;
        } catch (err) {
            console.log(err);
            throw err;
        }
    }

    static async setForgetPassword(data){
        try {
            const user = await UserStore.find(data);
            user.setNewPassword(data.password);
            const updatedUser = await passwordHandler.generatePassword(user);
            await UserStore.update(updatedUser);
            return user;
        } catch (err) {
            console.log(err);
            throw err;
        }
    }

    static async resetPassword(data){
        try {
            const user = await UserStore.findByPhoneNumber(data);
            await passwordHandler.verifyPassword(data.oldPassword, user);
            user.setNewPassword(data.password);
            const updateUser = await passwordHandler.generatePassword(user);
            await UserStore.update(updateUser);

            return true;
        } catch (err) {
            console.log(err);
            throw err;
        }
    }

    static async emailVerification(code){
        try {
            const emailConfirmation = await EmailConfirmation.updateStatus(code, true);
            const user = await UserStore.find(emailConfirmation);
            await Notification.emailVerification(user);

            return true;
        } catch (err) {
            console.log(err);
            throw err;
        }
    }

    static async socialLogin(email) {
        return await UserStore.findByEmail({email});
    }

    static async getUserDetails(userId){
        try {
            const user = await UserStore.find({userId});
            const jwtToken = await auth.generateToken(user);

            user.setJwtToken(jwtToken);

            return user;
        } catch (err) {
            console.log(err);
            throw err;
        }
    }

    static async updateUser(data){
        try {
            await UserStore.update(data);
            return true;
        } catch (err) {
            console.log(err);
            throw err;
        }
    }
}

module.exports = UserService;
