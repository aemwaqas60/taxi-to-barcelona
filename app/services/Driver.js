const DriverStore = require('../store/Driver');
const DriverEntity = require('../entities/Driver');
const UploadFile = require('./UploadFile');
const uuid = require('uuid/v1');

class DriverService {
    static async createDriver(driver, driverImage){
        try {
            const driverPhoto = await UploadFile.uploadFile(driverImage);
            const driverObj = DriverEntity.createDriver(uuid(), driver.name, driver.email, driver.idCard, driver.licenseNumber, driver.insuranceNumber,
                                                    driver.countryCode, driver.phoneNumber, driverPhoto);

            return await DriverStore.add(driverObj);
        } catch (err) {
            throw err;
        }
    }
}

module.exports = DriverService;
