const passport = require('passport');
const FacebookStrategy = require('passport-facebook').Strategy;
const config = require('../../config/config');


passport.serializeUser(function(profile, done) {
  done(null, profile.id);
});

passport.deserializeUser(function(id, done) {
    done({}, {});
});

passport.use(new FacebookStrategy({

    // pull in our app id and secret from our auth.js file
    clientID        : config.facebookAuth.clientID,
    clientSecret    : config.facebookAuth.clientSecret,
    callbackURL     : config.facebookAuth.callbackURL,
    profileFields: ['id', 'displayName', 'email'],

    },
    async function(token, refreshToken, profile, done) {
        return done(null, profile);
}));

let FacebookRoutes = {
    loginAuthenticate: () => {
        return passport.authenticate('facebook', { scope: ['email'], state: 'login'});
    },
    authenticate: () => {
        return passport.authenticate('facebook', { scope: ['email'] });
      },
    callback: () => {
        return passport.authenticate('facebook', {failureRedirect: `${config.appBaseURL}/register`});
      }
};
module.exports = FacebookRoutes;
