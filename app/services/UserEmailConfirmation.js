const EmailConfirmation = require('../store/EmailConfirmation');
const shortid = require('shortid');
const EmailConfirmationEntity = require('../entities/EmailConfirmation');

class UserEmailConfirmation {
    static async createConfirmation(user) {
        try {
            const emailConfirmationObj = EmailConfirmationEntity.createEmailConfirmation(shortid.generate(), false ,user.userId);
            return await EmailConfirmation.add(emailConfirmationObj);
        } catch (err) {
            console.log(err);
            return err;
        }

        return true;
    }
}

module.exports = UserEmailConfirmation;