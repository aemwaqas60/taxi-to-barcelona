const SendEmail = require('./sendEmail');
const moment = require('moment');

const emailSubject = {
    welcomeConfirmationUser:'Confirm your account -- Welcome to Taxitobacelona.com',
    thankYouForConfirmation:'Thank You For Confirming Your Account',
    forgetPassword:'Reset Your Password',
    taxiBooked: 'Your TaxiToBarcelona order for your trip in Barcelona -- ',
    taxiCanceled: 'Your TaxiToBarcelona order for your trip has been canceled -- ',
    notifyDriverForCancelBooking: 'Customer canceled the transfer on ',
    driverAssigned: 'Driver assigned for your TaxiToBarcelona transfer on ',
    notifyDriverForNewBooking: 'New Booking Assigned on ',
    driverArrived: 'Notice: Your driver has arrived at the pickup location -- ',
    bookingCompleted: 'Thank you for riding with us -- ',
};

class Notification {
    static async bookingCreated(booking, user) {
        await SendEmail.send(user.email, `${emailSubject.taxiBooked}${booking.bookingRef}`, 'taxiBooked', {booking, user});

        return true;
    }

    static async cancelBooking(booking, refundAmount) {
        if(booking.driver !== null) {
            await SendEmail.send(booking.driver.email, `${emailSubject.notifyDriverForCancelBooking}${booking.bookingRef}`, 'NotifyDriverForCancelBooking', booking);
        }
        await SendEmail.send(booking.user.email, `${emailSubject.taxiCanceled}${booking.bookingRef}`, 'taxiCanceled', {booking, refundAmount});

        return true;
    }

    static async driverArrived(booking) {
        await SendEmail.send(booking.user.email, `${emailSubject.driverArrived} ${booking.bookingRef}`, 'driverArrived', booking);

        return true;
    }

    static async bookingCompleted(booking) {
        await SendEmail.send(booking.user.email, `${emailSubject.bookingCompleted} ${booking.bookingRef}`, 'bookingCompleted', booking);

        return true;
    }


    static async driverAssigned(booking, driver) {
        const subjectInfo = `${moment(booking.pickupTime).format("dddd, MMMM Do YYYY")} - ${booking.bookingRef}`;

        await SendEmail.send(booking.user.email, `${emailSubject.driverAssigned} ${subjectInfo}`, 'driverAssigned', {booking, driver});
        await SendEmail.send(driver.email, `${emailSubject.notifyDriverForNewBooking} ${subjectInfo}`, 'notifyDriverForNewBooking', {booking, driver});

        return true;
    }

    static async userSignUp(user) {
        await SendEmail.send(user.email, emailSubject.welcomeConfirmationUser, 'welcomeConfirmationUser', user);

        return true;
    }

    static async forgetPassword(user) {
        await SendEmail.send(user.email, emailSubject.forgetPassword, 'forgetPassword', user);

        return true;
    }

    static async emailVerification(user) {
        await SendEmail.send(user.email, emailSubject.thankYouForConfirmation, 'thankYouForConfirmingYourAccount', {});

        return true;
    }
}

module.exports = Notification;