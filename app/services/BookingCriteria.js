const BookingCriteriaStore = require('../store/BookingCriteria');
const BookingCriteriaEntity = require('../entities/BookingCriteria');

class BookingCriteriaService {
    static async addBookingCriteria(bookingCriteria){
        try {
            const bookingCriteriaObj = BookingCriteriaEntity.createBookingCriteria(bookingCriteria.bookingCriteriaId, bookingCriteria.pickUpDate, bookingCriteria.passenger,
                bookingCriteria.luggage, bookingCriteria.passengerName, bookingCriteria.cost, bookingCriteria.carType, bookingCriteria.wheelChair, bookingCriteria.flightNumber,
                bookingCriteria.bookingNote, bookingCriteria.termsOfService, bookingCriteria.fromAddress, bookingCriteria.toAddress);

            return await BookingCriteriaStore.add(bookingCriteriaObj);
        } catch (err) {
            throw err;
        }
    }

    static async updateBookingCriteria(bookingCriteria){
        try {
            const bookingCriteriaObj = BookingCriteriaEntity.createBookingCriteria(bookingCriteria.bookingCriteriaId, bookingCriteria.pickUpDate, bookingCriteria.passenger,
                bookingCriteria.luggage, bookingCriteria.passengerName, bookingCriteria.cost, bookingCriteria.carType, bookingCriteria.wheelChair, bookingCriteria.flightNumber,
                bookingCriteria.bookingNote, bookingCriteria.termsOfService, bookingCriteria.fromAddress, bookingCriteria.toAddress);
            
            return await BookingCriteriaStore.update(bookingCriteriaObj);
        } catch (err) {
            throw err;
        }
    }

    static async getBookingCriteria(bookingCriteriaId){
        try {
            return await BookingCriteriaStore.find(bookingCriteriaId);
        } catch (err) {
            throw err;
        }
    }
}

module.exports = BookingCriteriaService;
