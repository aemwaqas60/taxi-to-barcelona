const {promisify} = require('util');
const uuid = require('uuid/v1');
const aws = require('aws-sdk');
var fs =  require('fs');
const readFileAsync = promisify(fs.readFile);

const config = require('../../config/config');

aws.config.update({
    secretAccessKey: config.S3.secret,
    accessKeyId: config.S3.key,
    region: config.S3.region,
});



class UploadFile {

    static async uploadFile(image) {
        try {
            const s3 = new aws.S3();
            const fileName = `${uuid()}-${image[0].originalname}`;
            const file = await readFileAsync(image[0].path);
            const params = {
                Bucket: config.S3.bucket,
                Key: `${fileName}`,
                ACL: 'public-read',
                ContentType: image[0].mimetype,
                Body: file };

            await s3.putObject(params).promise();

            return fileName;
        } catch (err) {
            console.log(err);
            throw err;
        }
    }
}

module.exports = UploadFile;