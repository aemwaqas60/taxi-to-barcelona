const _ = require("lodash");
const distance = require('google-distance');
const config = require('../../config/config');
distance.apiKey = config.googleMapApiKey;
const PricingTableStore = require('../store/PricingTable');
const quoteConfig = config.bookingQuote;
const QuoteEntity = require('../entities/Quote');
const BookingCriteriaStore = require('../store/BookingCriteria');

class QuoteService {
    static findDayTime(time){
        const hours = new Date(time).getHours();

        if(hours >= quoteConfig.nightHoursStart || hours <= quoteConfig.nightHoursEnd) {
            return 'night';
        }

        return 'day';
    }

    static async getDistance(pickupLng, pickupLat, dropoffLng, dropoffLat) {
        return new Promise ((resolve, reject) => {
            distance.get({ index: 1, origin: `${pickupLng}, ${pickupLat}`, destination: `${dropoffLng}, ${dropoffLat}`},
                function(err, data) {
                    if (err) {
                        console.log(err);
                        reject(err);
                    }
                    resolve(parseInt(data.distance));
                });
        });
    }

    static async calculateCost(totalDistance, carTypeCode, time) {
        // type will be day/night
        let cost = '';
        const pricingTimeType = QuoteService.findDayTime(time);

        const pricingData = await PricingTableStore.find(carTypeCode, pricingTimeType);
        let distanceFee = _.round(pricingData.pricePerKm*totalDistance, 2);

        if(totalDistance < quoteConfig.minDistanceKM) {
            cost = pricingData.priceMin;
        } else {
            cost = (distanceFee > pricingData.priceMin) ? distanceFee : pricingData.priceMin;
        }

        return QuoteEntity.createQuote(cost, distanceFee, carTypeCode);
    }

    static async calculateCarsQuote(distance, passengers, time) {
        try {
            let carType = [];

            if(passengers > 4 && passengers <= 7) {
                carType = ['MPV'];
            } else if(passengers <= 4) {
                carType = ['EXEC', 'SALOON'];
            }

            return await Promise.all(carType.map(async (carTypeCode) => {
                return await QuoteService.calculateCost(distance, carTypeCode, time);
            }));
        } catch (err) {
            console.log(err);
            return err;
        }
    }

    static async getCarsQuote(bookingCriteriaId, carType){
        try {
            let quote = '' ;
            const bookingCriteria = await BookingCriteriaStore.find(bookingCriteriaId);
            const distance = await QuoteService.getDistance(bookingCriteria.pickUpLng, bookingCriteria.pickUpLat, bookingCriteria.dropOffLng, bookingCriteria.dropOffLat);

            if(typeof carType !== 'undefined' && carType !== null) {
                quote = await QuoteService.calculateCost(distance, carType, bookingCriteria.pickupTime);
            } else {
                quote = await QuoteService.calculateCarsQuote(distance, bookingCriteria.passengerTotal, bookingCriteria.pickupTime);
            }

            return quote;
        }
         catch (err) {
            console.log(err);
            return err;
        }
    }


    static async getTripCost(booking){
        try {
            const totalDistance = await QuoteService.getDistance(booking.pickupLng, booking.pickupLat, booking.dropoffLng, booking.dropoffLat);
            const cost = await QuoteService.calculateCost(totalDistance, booking.carType, booking.pickupTime);
            return cost;
        } catch (err) {
            console.log(err);
            return err;
        }
    }
}

module.exports = QuoteService;