const nodemailer = require("nodemailer");
const config = require('../../config/config');
const path = require('path');
const pug = require('pug');

class SendEmail {
    static async send(to, subject, template, data){
        try {
            data.baseURL = config.baseURL;
            data.appURL = config.appBaseURL;
            const emailPage =  pug.renderFile(path.join(__dirname, '../../emailTemplates/' + template + '.pug'), {
                data,
            });

            const transporter = nodemailer.createTransport({
                host: config.alertEmail.host,
                port: config.alertEmail.port,
                auth: config.alertEmail.auth,
                secure: false,
                requireTLS: true,
            });

            // setup e-mail data with unicode symbols
            const mailOptions = {
                from: config.alertEmail.from,
                to,
                subject,
                html: emailPage
            };

            const info = await transporter.sendMail(mailOptions);
            console.log(info.envelope);
        } catch (err) {
            console.log(err);
        }
    }
}

module.exports = SendEmail;
