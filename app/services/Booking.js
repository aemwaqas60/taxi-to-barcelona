const BookingStore = require('../store/Booking');
const BookingFactory = require('../entities/BookingFactory');
const DriverStore = require('../store/Driver');
const Quote = require('../services/Quote');
const CarStore = require('../store/Car');
const PaymentService = require('../services/Payment');
const Notification = require('./Notification');
const UserStore = require('../store/User');

class BookingService {

    static async getUserBooking(userId, pageNumber) {
        try {
            return await BookingStore.findByUserId(userId, pageNumber);
        } catch(err) {
            console.log(err);
            return err;
        }
    }

    static async getBooking(bookingId) {
        try {
          return await BookingStore.findById(bookingId);
        } catch(err) {
            console.log(err);
            return err;
        }
    }


    static async createBooking(data, user){
        try {
            const booking = BookingFactory.createFromClientData(data);
            const quote = await Quote.getTripCost(booking);

            booking.setCostAndDistanceFee(quote.cost, quote.distanceFee);
            const payment = await PaymentService.addPayment(user, booking, data.stripeToken);

            booking.setPayment(payment);
            const result = await BookingStore.add(booking);
            await Notification.bookingCreated(result, user);
            return true;
        } catch (err) {
            console.log(err);
            return err;
        }
    }

    static async cancelBooking(bookingId){
        try {
            const booking = await BookingStore.findById(bookingId);
            booking.markBookingCanceled();

            const refundAmount = await PaymentService.refundPayment(booking);
            await BookingStore.update(booking);

            // Send notification to driver and client
            await Notification.cancelBooking(booking, refundAmount);

            return true;
        } catch (err) {
            console.log(err);
            return err;
        }
    }

    static async assignDriverToBooking(bookingId, driverId) {
        try {
            const booking = await BookingStore.findById(bookingId);
            const driver = await DriverStore.findById(driverId);

            booking.setDriver(driver);
            await BookingStore.update(booking);

            // send email to driver and client
            await Notification.driverAssigned(booking, driver);

            return true;
        } catch (err) {
            throw err;
        }
    }

    static async updateDriverArriveStatus(bookingId) {
        try {
            const booking = await BookingStore.findById(bookingId);

            booking.driverHasArrived();
            await BookingStore.update(booking);

            // send email to client
            await Notification.driverArrived(booking);
            return true;
        } catch (err) {
            console.log(err);
            return err;
          }
        }

    static async updateBookingCompleteStatus(bookingId) {
        try {
            const booking = await BookingStore.findById(bookingId);

            booking.markBookingComplete();
            await BookingStore.update(booking);

            // send email to client
            await Notification.bookingCompleted(booking);
            return true;
        } catch (err) {
            console.log(err);
            return err;
        }
    }
}

module.exports = BookingService;
