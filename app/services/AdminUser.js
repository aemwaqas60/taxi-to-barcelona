const uuid = require('uuid/v1');

const AdminUserStore = require('../store/AdminUser');
const AdminUser = require('../entities/AdminUser');
const passwordHandler = require('../../controllers/auth/verifyPassword');
const auth = require('../../controllers/auth/authMiddleware');


class AdminUserService {
    static async createNewUser(adminUser){
        try {
            const clientAdminUserObj = await AdminUser.createAdminUser(uuid(), adminUser.name, adminUser.email, adminUser.password);
            const adminUserObj = await AdminUserStore.add(clientAdminUserObj);

            const jwtToken = await auth.generateToken(adminUserObj);
            adminUserObj.setJwtToken(jwtToken);

            return adminUserObj;
        } catch (err) {
            throw err;
        }
    }

    static async login(adminUserObj){
        try {
            const adminUser = await AdminUserStore.findByEmail(adminUserObj);
            await passwordHandler.verifyPassword(adminUserObj.password, adminUser);

            const jwtToken = await auth.generateToken(adminUser);
            adminUser.setJwtToken(jwtToken);

            return adminUser;
        } catch (err) {
            throw err;
        }
    }
}

module.exports = AdminUserService;
