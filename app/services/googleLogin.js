const Passport = require('passport');
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const config = require('../../config/config');

Passport.serializeUser(function(profile, done) {
    done(null, profile.id);
});

Passport.deserializeUser(function(id, done) {
    done({}, {});
});

Passport.use(new GoogleStrategy({
        // pull in our app id and secret from our auth.js file
        clientID: config.googleAuth.clientID,
        clientSecret: config.googleAuth.clientSecret,
        callbackURL: config.googleAuth.callbackURL,
    },
    async function(token, refreshToken, profile, done) {
            return done(null, profile);
}));

let GoogleRoutes = {
    loginAuthenticate: () => {
        return Passport.authenticate('google', { scope: ['email', 'profile'], state: 'login'});
    },
    authenticate: () => {
        return Passport.authenticate('google', { scope: ['email', 'profile']});
    },
    callback: () => {
        return Passport.authenticate('google', {failureRedirect: `${config.appBaseURL}/register`});
    }
};
module.exports = GoogleRoutes;
