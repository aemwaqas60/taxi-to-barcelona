const config = require('../../config/config');

class Driver {
    constructor(driverId, name, email, idCard, licenseNumber, insuranceNumber, countryCode, phoneNumber, photo) {
        this.driverId = driverId;
        this.name = name;
        this.email = email;
        this.idCard = idCard;
        this.licenseNumber = licenseNumber;
        this.insuranceNumber = insuranceNumber;
        this.countryCode = countryCode;
        this.phoneNumber = phoneNumber;
        this.photo = photo;
    }

    setDriverPhoto(photo) {
        this.photo = `${config.S3.url}${photo}`;

        return this;
    }


    static createDriver(driverId, name, email, idCard, licenseNumber, insuranceNumber, countryCode, phoneNumber, photo) {
        return new Driver(driverId, name, email, idCard, licenseNumber, insuranceNumber, countryCode, phoneNumber, photo);
    }

    static createFromObject(obj){
        const driver =  new Driver(obj.driverId, obj.name, obj.email, obj.idCard, obj.licenseNumber, obj.insuranceNumber,
                                                        obj.countryCode, obj.phoneNumber);
        driver.setDriverPhoto(obj.photo);

        return driver;
    }

    setCar(car) {
        this.car = car;
        return this;
    }

    static createFromDataBase(driver, car) {
        const driverObj = Driver.createFromObject(driver);
        driverObj.setCar(car);

        return driverObj;
    }
}

module.exports = Driver;