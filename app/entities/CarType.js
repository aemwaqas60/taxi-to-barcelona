
class CarType {
    constructor(id, name, code) {
        this.id = id;
        this.name = name;
        this.code = code;
    }

    static createFromObject(obj){
        return  new CarType(obj.id, obj.name, obj.code);
    }
}

module.exports = CarType;