
class Car {
    constructor(model, type, make, seats, bags, carNumber, driverId) {
        this.model = model;
        this.type = type;
        this.make = make;
        this.seats = seats;
        this.bags = bags;
        this.carNumber = carNumber;
        this.driverId = driverId;
    }

    toObject() {
        return JSON.parse(JSON.stringify(this));
    }

    static createCar(model, type, make, seats, bags, carNumber, driverId) {
        return new Car(model, type, make, seats, bags, carNumber, driverId);
    }

    static createFromObject(obj){
        return  new Car(obj.model, obj.type, obj.make, obj.seats, obj.bags, obj.carNumber, obj.driverId);
    }
}

module.exports = Car;