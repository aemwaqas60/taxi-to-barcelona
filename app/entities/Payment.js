
class Payment {
    constructor(id, transactionId, bookingId, chargedAmount, refunded) {
        this.id = id;
        this.transactionId = transactionId;
        this.bookingId = bookingId;
        this.chargedAmount = chargedAmount;
        this.refunded = refunded;
    }

    setRefundStatus(refundStatus) {
        this.refunded = refundStatus;
        return this;
    };

    static createPayment(id, transactionId, bookingId, chargedAmount, refunded) {
        return new Payment(id, transactionId, bookingId, chargedAmount, refunded);
    }

    static createFromObject(obj){
        return new Payment(obj.id, obj.transactionId, obj.bookingId, obj.chargedAmount, obj.refunded);
    }
}

module.exports = Payment;