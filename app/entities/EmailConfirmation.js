
class EmailConfirmation {
    constructor(confirmationCode, confirmed, userId) {
        this.confirmationCode = confirmationCode;
        this.confirmed = confirmed;
        this.userId = userId;
    }

    toObject() {
        return JSON.parse(JSON.stringify(this));
    }

    static createEmailConfirmation(code, confirmed, userId) {
        return new EmailConfirmation(code, confirmed, userId);
    }

    static createFromObject(obj){

        return new EmailConfirmation(obj.confirmationCode, obj.confirmed, obj.userId);
    }
}

module.exports = EmailConfirmation;