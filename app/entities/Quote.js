
class Quote {
    constructor(cost, distanceFee, carType) {
        this.cost = cost;
        this.distanceFee = distanceFee;
        this.carType = carType;
    }

    static createQuote(cost, distanceFee, carType) {
        return new Quote(cost, distanceFee, carType);
    }
}

module.exports = Quote;