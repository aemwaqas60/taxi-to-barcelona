
class PricingTable {
    constructor(type, pricePerKm, priceMin, carTypeCode) {
        this.type = type;
        this.pricePerKm = pricePerKm;
        this.priceMin = priceMin;
        this.carTypeCode =  carTypeCode;
    }

    toObject() {
        return JSON.parse(JSON.stringify(this));
    }

    static createPricing(type, pricePerKm, priceMin, carTypeCode) {
        return new PricingTable(type, pricePerKm, priceMin, carTypeCode);
    }

    static createFromObject(obj){
        const pricing = new PricingTable(obj.type, obj.pricePerKm, obj.priceMin, obj.carTypeCode);
        return pricing;
    }
}

module.exports = PricingTable;