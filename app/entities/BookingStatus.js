const BookingStatusType = {
    PENDING_DRIVER: 'PENDING_DRIVER',
    CANCELED: 'CANCELED',
    DRIVER_ASSIGNED: 'DRIVER_ASSIGNED',
    DRIVER_ARRIVED: 'DRIVER_ARRIVED',
    COMPLETED: 'COMPLETED',
};

class BookingStatus {
    static get PENDING_DRIVER(){
        return BookingStatusType.PENDING_DRIVER;
    }

    static get CANCELED(){
        return BookingStatusType.CANCELED;
    }

    static get DRIVER_ASSIGNED(){
        return BookingStatusType.DRIVER_ASSIGNED;
    }

    static get DRIVER_ARRIVED(){
        return BookingStatusType.DRIVER_ARRIVED;
    }

    static get COMPLETED(){
        return BookingStatusType.COMPLETED;
    }
}

module.exports = BookingStatus;