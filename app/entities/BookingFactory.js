const Booking= require('./Booking');
const BookingStatus = require('./BookingStatus');
const shortid = require('shortid');

class BookingFactory {
    static createFromClientData(obj) {
        const booking = new Booking(obj.bookingId, obj.passengerName, obj.pickupName, obj.pickupAddress, obj.pickupLng, obj.pickupLat, obj.dropoffName,
            obj.dropoffAddress, obj.dropoffLng, obj.dropoffLat, obj.wheelchair, obj.flightNumber, obj.pickupTime, obj.passengerTotal, obj.casesTotal,
            obj.carType, obj.notes, shortid.generate(), obj.userId);
        booking.assignDefault();
        return booking;
    };

    static createFromDataBase(dbObj, payment, driver, user) {
        const booking = new Booking(dbObj.bookingId, dbObj.passengerName, dbObj.pickupName, dbObj.pickupAddress, dbObj.pickupLng, dbObj.pickupLat, dbObj.dropoffName,
            dbObj.dropoffAddress, dbObj.dropoffLng, dbObj.dropoffLat, dbObj.wheelchair, dbObj.flightNumber, dbObj.pickupTime, dbObj.passengerTotal, dbObj.casesTotal,
            dbObj.carType, dbObj.notes, dbObj.bookingRef, dbObj.userId);

        booking.setCostAndDistanceFee(dbObj.cost, dbObj.distanceFee);
        booking.setPayment(payment);
        booking.setDriver(driver);
        booking.setUser(user);

        if(dbObj.bookingStatus === BookingStatus.DRIVER_ARRIVED) {
            booking.driverHasArrived();
        }
        if(dbObj.bookingStatus === BookingStatus.COMPLETED) {
            booking.markBookingComplete();
        }

        return booking;
    }
}

module.exports = BookingFactory;