const BookingStatus = require('./BookingStatus');
const PaymentStatus = require('./PaymentStatus');

class Booking {
    constructor(bookingId, passengerName, pickupName, pickupAddress, pickupLng, pickupLat, dropoffName, dropoffAddress, dropoffLng, dropoffLat,
                wheelchair, flightNumber, pickupTime, passengerTotal, casesTotal, carType, notes, bookingRef, userId) {
        this.bookingId = bookingId;
        this.passengerName = passengerName;
        this.pickupName = pickupName;
        this.pickupAddress = pickupAddress,
        this.pickupLng = pickupLng,
        this.pickupLat = pickupLat,
        this.dropoffName = dropoffName;
        this.dropoffAddress = dropoffAddress;
        this.dropoffLng = dropoffLng;
        this.dropoffLat = dropoffLat;
        this.wheelchair = wheelchair;
        this.flightNumber =  flightNumber;
        this.pickupTime = pickupTime;
        this.passengerTotal = passengerTotal;
        this.casesTotal = casesTotal;
        this.carType = carType;
        this.notes = notes;
        this.bookingRef = bookingRef;
        this.userId = userId;
    }

    assignDefault(){
        this.bookingStatus = BookingStatus.PENDING_DRIVER;
        this.paymentStatus = PaymentStatus.PAID;
        this.driver = null;
        this.cost = null;
        this.distanceFee = null;
        this.payment = null;

        return this;
    };

    setCostAndDistanceFee(cost, distanceFee){
        this.cost = cost;
        this.distanceFee = distanceFee;

        return this;
    };

    driverHasArrived(){
        if(this.paymentStatus === PaymentStatus.PAID) {
            this.bookingStatus = BookingStatus.DRIVER_ARRIVED;
            return this;
        }
        throw new Error('Payment is not charged');
    }

    markBookingCanceled(){
        if(this.bookingStatus !== BookingStatus.CANCELED && this.bookingStatus !== BookingStatus.COMPLETED){
            this.paymentStatus = PaymentStatus.REFUNDED;
            this.bookingStatus = BookingStatus.CANCELED;
            return this;
        }
        throw new Error('This booking is already canceled or completed');
    }

    markBookingComplete(){
        if(this.paymentStatus === PaymentStatus.PAID && this.bookingStatus === BookingStatus.DRIVER_ARRIVED) {
            this.bookingStatus = BookingStatus.COMPLETED;
            return this;
        }
        throw new Error('Payment is not charged or Driver has not arrived');
    }

    /**
     *
     * @param {Payment} payment
     */
    setPayment(payment){
        this.payment = payment;
        this.paymentStatus = PaymentStatus.PAID;
        if(payment.refunded === true){
            this.paymentStatus = PaymentStatus.REFUNDED;
            this.bookingStatus = BookingStatus.CANCELED;
        }
    }

    setDriver(driver){
        if(driver === null) {
            this.bookingStatus = BookingStatus.PENDING_DRIVER;
            return this;
        }

        if(this.paymentStatus === PaymentStatus.PAID || this.paymentStatus === PaymentStatus.REFUNDED) {
            this.driver = driver;
            this.driverId = driver.driverId;
            this.bookingStatus = BookingStatus.DRIVER_ASSIGNED;

            return this;
        }
        throw new Error('Assigning Driver before payment');
    };

    setUser(user) {
        this.user = user;

        return this;
    }

    toDBObject() {
        const bookingObj = JSON.parse(JSON.stringify(this));
        bookingObj.driverId = bookingObj.driver ? bookingObj.driver.driverId : null;
        bookingObj.paymentId = bookingObj.payment.id;
        delete bookingObj.driver;
        delete bookingObj.payment;
        return bookingObj;
    }
}

module.exports = Booking;