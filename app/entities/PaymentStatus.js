const PaymentStatusType =
    {
        PENDING_PAYMENT: 'PENDING_PAYMENT',
        REFUNDED: 'REFUNDED',
        PAID: 'PAID',
    };

class PaymentStatus {
    static get PENDING_PAYMENT(){
        return PaymentStatusType.PENDING_PAYMENT;
    }

    static get REFUNDED(){
        return PaymentStatusType.REFUNDED;
    }

    static get PAID(){
        return PaymentStatusType.PAID;
    }
}

module.exports = PaymentStatus;