
class BookingCriteria {
    constructor(bookingCriteriaId, pickupTime, passengerTotal, casesTotal, passengerName, cost, carType, wheelChair, flightNumber, bookingNote, termsOfService) {
        this.bookingCriteriaId = bookingCriteriaId;
        this.pickupTime = pickupTime;
        this.passengerTotal = passengerTotal;
        this.casesTotal = casesTotal;
        this.passengerName = passengerName;
        this.cost = cost;
        this.carType = carType;
        this.wheelChair = wheelChair;
        this.flightNumber = flightNumber;
        this.bookingNote = bookingNote;
        this.termsOfService = termsOfService;
    }


    setPickUpDetails(pickUpDetails) {
        this.pickUpAddress = pickUpDetails.place;
        this.pickUpId = pickUpDetails.placeId;
        this.pickUpLat = pickUpDetails.lat;
        this.pickUpLng = pickUpDetails.lng;

        return this;
    }

    setDropOffDetails(dropOffDetails) {
        this.dropOffAddress = dropOffDetails.place;
        this.dropOffId = dropOffDetails.placeId;
        this.dropOffLng = dropOffDetails.lng;
        this.dropOffLat = dropOffDetails.lat;

        return this;
    }

    setAddress(place,  placeId, lng, lat){
        return {place, placeId, lng, lat};
    }

    static createBookingCriteria(bookingCriteriaId, pickupTime, passengerTotal, casesTotal, passengerName, cost, carType,
                                 wheelChair, flightNumber, bookingNote, termsOfService, pickUpDetails, dropOffDetails) {
        const bookingCriteria = new BookingCriteria(bookingCriteriaId, pickupTime, passengerTotal, casesTotal,passengerName, cost, carType,
                                                    wheelChair, flightNumber, bookingNote, termsOfService,);
        bookingCriteria.setPickUpDetails(pickUpDetails);
        bookingCriteria.setDropOffDetails(dropOffDetails);

        return bookingCriteria;
    }

    static createFromObject(obj){
        const bookingCriteria = new BookingCriteria(obj.bookingCriteriaId, obj.pickupTime, obj.passengerTotal, obj.casesTotal, obj.passengerName,
                                                    obj.cost, obj.carType, obj.wheelChair, obj.flightNumber, obj.bookingNote, obj.termsOfService);
        bookingCriteria.fromAddress = bookingCriteria.setAddress(obj.pickUpAddress, obj.pickUpId, obj.pickUpLng, obj.pickUpLat);
        bookingCriteria.toAddress = bookingCriteria.setAddress(obj.dropOffAddress, obj.dropOffId, obj.dropOffLng, obj.dropOffLat);
        return bookingCriteria;
    }
}

module.exports = BookingCriteria;