const passwordHandler = require('../../controllers/auth/verifyPassword');

class User {
    constructor(userId, name, email, password, salt, phoneNumber, countryCode, loginType) {
        this.userId = userId;
        this.name = name;
        this.email = email;
        this.password = password;
        this.salt = salt;
        this.phoneNumber = phoneNumber;
        this.countryCode = countryCode;
        this.loginType = loginType;
    }

    toObject() {
        return JSON.parse(JSON.stringify(this));
    }
    /**
     *
     * @param {EmailConfirmation} emailConfirmation
     * @returns {User}
     */

    setEmailConfirmationCode(emailConfirmation){
        this.emailConfirmation = emailConfirmation;

        return this;
    }

    /**jwtToken
     *
     * @param {String} jwtToken
     * @returns {User}
     */

    setJwtToken(jwtToken){
        this.jwtToken = jwtToken;

        return this;
    }

    async setPassword(user) {
        const result = await passwordHandler.generatePassword(user);
        this.password = result.password;
        this.salt = result.salt;
        return this;
    }

    setNewPassword(pwd){
        this.password = pwd;

        return this;
    }

    static async createUser(userId, name, email, password, salt, phoneNumber, countryCode, loginType) {
        const user = new User(userId, name, email, password, salt, phoneNumber, countryCode, loginType);
        await user.setPassword(user);
        return user;
    }

    static createFromObject(obj){
        const user = new User(obj.userId, obj.name, obj.email, obj.password, obj.salt, obj.phoneNumber, obj.countryCode, obj.loginType);

        return user;
    }
}

module.exports = User;