const passwordHandler = require('../../controllers/auth/verifyPassword');

class AdminUser {
    constructor(adminUserId, name, email, password, salt) {
        this.adminUserId = adminUserId;
        this.name = name;
        this.email = email;
        this.password = password;
        this.salt = salt;
    }

    async setPassword(adminUser) {
        const result = await passwordHandler.generatePassword(adminUser);
        this.password = result.password;
        this.salt = result.salt;

        return this;
    }

    setJwtToken(jwtToken) {
        this.jwtToken = jwtToken;

        return this;
    }

    static async createAdminUser(adminUserId, name, email, password) {
        const adminUser = new AdminUser(adminUserId, name, email, password);
        await adminUser.setPassword(adminUser);
        return adminUser;
    }

    static createFromObject(obj){
        const adminUser = new AdminUser(obj.adminUserId, obj.name, obj.email, obj.password, obj.salt);

        return adminUser;
    }
}

module.exports = AdminUser;