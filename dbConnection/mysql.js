const Sequelize = require('sequelize');
const config = require('../config/config');

const { SQLdb } = config;

const sequelize = new Sequelize(SQLdb.database, SQLdb.username, SQLdb.password, SQLdb);

sequelize.authenticate()
    .then(() => {
        // console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });

module.exports = sequelize;