const express = require('express');
const router = express.Router({});

const adminUserController = require('../controllers/adminUser');
const BookingController = require('../controllers/booking');
const CarController = require('../controllers/car');
const CarTypeController = require('../controllers/carType');
const DriverController = require('../controllers/driver');
const auth = require('../controllers/auth/authMiddleware');

router.post('/booking/assignBooking', auth.isAdmin, BookingController.assignDriver);
router.post('/car/add', auth.isAdmin, CarController.addCar);
router.get('/carType/get', auth.isAdmin, CarTypeController.getCarTypes);
router.post('/driver/add', auth.isAdmin, DriverController.addDriver);
router.post('/signUp', adminUserController.signUp);
router.post('/login', adminUserController.login);


module.exports = router;
