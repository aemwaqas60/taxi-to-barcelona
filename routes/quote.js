const express = require('express');

const router = express.Router({});
const QuoteController = require('../controllers/quote');

router.get('/quote/get', QuoteController.getQuote);


module.exports = router;
