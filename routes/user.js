const express = require('express');

const router = express.Router({});
const UserController = require('../controllers/user');
const auth = require('../controllers/auth/authMiddleware');
const facebookPassport = require('../app/services/facebookLogin');
const googlePassport = require('../app/services/googleLogin');

router.post('/user/signup', UserController.signup);
router.post('/user/login', UserController.login);
router.get('/user/handshake', auth.isAuthorized, UserController.handshake);
router.post('/user/forgetPassword', UserController.forgetPassword);
router.post('/user/resetPassword', UserController.restPassword);
router.post('/user/setForgetPassword', UserController.setForgetPassword);
router.put('/user/updateUser', auth.isAuthorized, UserController.updateUser);
router.get('/user/emailVerification', UserController.emailVerification);

// Social Login
router.get('/user/signupFacebook', facebookPassport.authenticate());
router.get('/user/loginFacebook', facebookPassport.loginAuthenticate());
router.get('/facebook/callback', facebookPassport.callback(), UserController.socialProfile);
router.get('/user/signupGoogle', googlePassport.authenticate());
router.get('/user/loginGoogle', googlePassport.loginAuthenticate());
router.get('/google/callback', googlePassport.callback(), UserController.socialProfile);
router.get('/user/userDetails', UserController.userDetails);

//  Email verification
router.get('/user/confirm', UserController.emailVerification);

module.exports = router;
