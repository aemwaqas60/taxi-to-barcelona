const express = require('express');

const router = express.Router({});
const BookingController = require('../controllers/booking');
const auth = require('../controllers/auth/authMiddleware');

router.post('/booking/create', auth.isAuthorized, BookingController.createBooking);
router.get('/booking/cancel/:bookingId', BookingController.cancelBooking);
router.get('/booking/user/get/:page', auth.isAuthorized, BookingController.getUserBooking);
router.get('/booking/get/:bookingId', BookingController.getBooking);
router.get('/booking/driverArrived/:bookingId', BookingController.driverArrived);
router.get('/booking/bookingCompleted/:bookingId', BookingController.bookingCompleted);

module.exports = router;
