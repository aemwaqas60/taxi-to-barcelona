const express = require('express');

const router = express.Router({});
const BookingCriteriaController = require('../controllers/bookingCriteria');

router.post('/bookingCriteria/create', BookingCriteriaController.createBookingCriteria);
router.put('/bookingCriteria/update', BookingCriteriaController.updateBookingCriteria);
router.get('/bookingCriteria/get/:bookingCriteriaId', BookingCriteriaController.getBookingCriteria);


module.exports = router;