const BookingCriteriaService = require('../app/services/BookingCriteria');
const BookingCriteriaRest = {};

BookingCriteriaRest.createBookingCriteria = async (req, res) => {
    try {
        const result = await BookingCriteriaService.addBookingCriteria(req.body);
        res.json(result);
    } catch(err) {
        console.log(err);
        res.status(500).json({message: 'Unable to create booking criteria'});
    }
};

BookingCriteriaRest.updateBookingCriteria = async (req, res) => {
    try {
        await BookingCriteriaService.updateBookingCriteria(req.body);
        res.json(true);
    } catch(err) {
        console.log(err);
        res.status(500).json({message: 'Unable to update booking criteria'});
    }
};

BookingCriteriaRest.getBookingCriteria = async (req, res) => {
    try {
        const result =await BookingCriteriaService.getBookingCriteria(req.params.bookingCriteriaId);
        res.json(result);
    } catch(err) {
        console.log(err);
        res.status(500).json({message: 'Unable to find booking criteria'});
    }
};

module.exports = BookingCriteriaRest;
