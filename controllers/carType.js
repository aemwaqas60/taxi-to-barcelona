const CarTypeService = require('../app/services/CarType');
const CarTypeRest = {};

CarTypeRest.getCarTypes = async (req, res) => {
    try {
        const result = await CarTypeService.getAllCarType();
        res.json(result);
    } catch(err) {
        console.log(err);
        res.status(500).json({message: 'Unable to find car types'});
    }
};

module.exports = CarTypeRest;
