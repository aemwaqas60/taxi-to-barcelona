const DriverService = require('../app/services/Driver');
const DriverRest = {};


DriverRest.addDriver = async (req, res) => {
    try {
        const result = await DriverService.createDriver(req.body, req.files);
        res.json(result);
    } catch(err) {
        console.log(err);
        res.status(500).json({message: 'Unable to add driver'});
    }
};

module.exports = DriverRest;
