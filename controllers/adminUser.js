const AdminUserService = require('../app/services/AdminUser');

const AdminUserRest = {};

AdminUserRest.signUp = async (req, res) => {
    try {
        const result = await AdminUserService.createNewUser(req.body);
        res.json(result);
    } catch(err) {
        console.log(err);
        res.status(500).json({message: 'Unable to add new admin user'});
    }
};

AdminUserRest.login = async (req, res) => {
    try {
        const result = await AdminUserService.login(req.body);
        res.json(result);
    } catch(err) {
        console.log(err);
        res.status(500).json({message: 'Unable to login'});
    }
};

module.exports = AdminUserRest;
