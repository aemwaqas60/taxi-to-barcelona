const jwt = require('jsonwebtoken');
const config = require('../../config/config');
const UserStore = require('../../app/store/User');
const AdminUserStore = require('../../app/store/AdminUser');

class AuthMiddleware {
    static async generateToken(data) {
        try {
            const email = data.email;
            if(email) {
                const token = await jwt.sign({email: email},
                    config.jwtSecret,
                    { expiresIn: '24h' // expires in 24 hours
                    }
                );
                return token;
            }
        } catch (err) {
            throw err;
        }
    }




    static async isAuthorized(req,res, next) {
        try {
            let token = req.headers['x-access-token'] || req.headers['authorization'];
            if (token.startsWith('Bearer ')) {
                // Remove Bearer from string
                token = token.slice(7, token.length);
            }

            if(token) {
                jwt.verify(token, config.jwtSecret, async (err, decoded) => {
                    if (err) {
                        console.log(err);
                        return res.status(401).json({ success: false, message: 'Failed to authenticate token'});
                    } else {
                        const data = {};
                        data.userId = req.headers['x-user-id'];
                        const user = await UserStore.find(data);
                        if(user) {
                          req.user = user;
                          req.decoded = decoded;
                          next();
                        }
                    }
                });
            } else {
                return res.status(401).send({ success: false, message: 'No token provided.'});
            }
        } catch (err) {
            throw err;
        }
    }

    static async isAdmin(req,res, next) {
        try {
            let token = req.headers['x-access-token'] || req.headers['authorization'];
            if (token.startsWith('Bearer ')) {
                // Remove Bearer from string
                token = token.slice(7, token.length);
            }

            if(token) {
                jwt.verify(token, config.jwtSecret, async (err, decoded) => {
                    if (err) {
                        // console.log(err);
                        return res.status(401).json({ success: false, message: 'Failed to authenticate token'});
                    } else {
                        const adminUserId = req.headers['x-user-id'];
                        const admin = await AdminUserStore.findById(adminUserId);
                        if(admin) {
                            req.admin = admin;
                            req.decoded = decoded;
                            next();
                        }
                    }
                });
            } else {
                return res.status(401).send({ success: false, message: 'No token provided.'});
            }
        } catch (err) {
            throw err;
        }
    }
}

module.exports = AuthMiddleware;
