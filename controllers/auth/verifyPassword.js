const crypto = require('crypto');

class PasswordHandling {

    static async generatePassword(userData) {
        try {
            const hash = crypto.createHash('sha256');

            userData.salt = crypto.randomBytes(16).toString('base64');
            hash.update(userData.password + userData.salt);
            userData.password = hash.digest('hex');
            return userData;
        } catch (err) {
            return err;
        }
    }

    static async verifyPassword(password, user) {
        const hash = crypto.createHash('sha256');

        hash.update(password + user.salt);
        const hashedPassword = hash.digest('hex');

        if (hashedPassword === user.password) {
            return true;
        }

        throw new Error('Incorrect Password');
    }
}

module.exports = PasswordHandling;
