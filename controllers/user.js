const UserService = require('../app/services/User');
const config = require('../config/config');

const UsersRest = {};

UsersRest.signup = async (req, res) => {
    try {
        const result = await UserService.createNewUser(req.body);
        res.json(result);
    } catch(err) {
        console.log(err);
        res.status(500).json({message: 'Unable to add new user'});
    }
};

UsersRest.login = async (req, res) => {
    try {
        const result = await UserService.login(req.body);
        res.json(result);
    } catch(err) {
        console.log(err);
        res.status(500).json({message: 'Unable to login'});
    }
};

UsersRest.handshake = async (req, res) => {
    res.json(req.user);
};

UsersRest.updateUser = async (req, res) => {
    try {
        await UserService.updateUser(req.body);
        res.json(true);
    } catch(err) {
        console.log(err);
        res.status(500).json({message: 'Unable to update user'});
    }
};

UsersRest.forgetPassword = async (req, res) => {
    try {
        await UserService.forgetPassword(req.body);
        res.json(true);
    } catch(err) {
        console.log(err);
        res.status(500).json({message: 'Unable to send email for forget password'});
    }
};

UsersRest.setForgetPassword = async (req, res) => {
    try {
        const user = await UserService.setForgetPassword(req.body);
        res.json(user);
    } catch(err) {
        console.log(err);
        res.status(500).json({message: 'Unable to update password'});
    }
};

UsersRest.restPassword = async (req, res) => {
    try {
        await UserService.resetPassword(req.body);
        res.json(true);
    } catch(err) {
        console.log(err);
        res.status(500).json({message: 'Unable to reset password'});
    }
};

UsersRest.emailVerification = async (req, res) => {
    try {
        await UserService.emailVerification(req.query.code);
        res.json(true);
    } catch(err) {
        console.log(err);
        res.status(500).json({message: 'Unable to complete user verification'});
    }
};

UsersRest.socialProfile = async (req, res) => {
    try {
        if(req.query.state === 'login') {
            const result = await UserService.socialLogin(req.user._json.email);
            res.redirect(`${config.appBaseURL}?userId=${result.userId}`);
        }

        return res.redirect(`${config.appBaseURL}/register?name=${req.user._json.name}&email=${req.user._json.email}`);
    } catch(err) {
        console.log(err);
        res.status(500).json({message: 'Unable to complete OAuth'});
    }
};

UsersRest.userDetails = async (req, res) => {
    try {
        const result = await UserService.getUserDetails(req.query.userId);
        return res.json(result);
    } catch(err) {
        console.log(err);
        res.status(500).json({message: 'Unable to find user details'});
    }
};


module.exports = UsersRest;
