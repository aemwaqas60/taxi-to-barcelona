const BookingService = require('../app/services/Booking');

const BookingRest = {};

BookingRest.createBooking = async (req, res) => {
    try {
        await BookingService.createBooking(req.body, req.user);
        res.json(true);
    } catch(err) {
        res.status(500).json({message: 'Unable to add booking and create charges'});
    }
};


BookingRest.cancelBooking = async (req, res) => {
    try {
        await BookingService.cancelBooking(req.params.bookingId);
        res.json(true);
    } catch (err) {
        res.status(500).json({message: 'Unable to cancel booking'});
    }
};

BookingRest.getUserBooking = async (req, res) => {
    try {
        res.json(await BookingService.getUserBooking(req.user.userId, req.params.page));
    } catch(err) {
        res.status(500).json({message: 'Unable to find Booking'});
    }
};

BookingRest.getBooking = async (req, res) => {
    try {
        res.json(await BookingService.getBooking(req.params.bookingId));
    } catch(err) {
        res.status(500).json({message: 'Unable to find Booking'});
    }
};

BookingRest.driverArrived = async (req, res) => {
    try {
        await BookingService.updateDriverArriveStatus(req.params.bookingId);
        res.json(true);
    } catch(err) {
        res.status(500).json({message: 'Unable to complete process for driver arrival'});
    }
};

BookingRest.bookingCompleted = async (req, res) => {
    try {
        res.json(await BookingService.updateBookingCompleteStatus(req.params.bookingId));
    } catch(err) {
        res.status(500).json({message: 'Unable to complete process for Booking completed'});
    }
};

BookingRest.assignDriver = async (req, res) => {
    try {
        res.json(await BookingService.assignDriverToBooking(req.body.bookingId, req.body.driverId));
    } catch(err) {
        console.log(err);
        res.status(500).json({message: 'Unable to assign driver to booking'});
    }
};

module.exports = BookingRest;














































