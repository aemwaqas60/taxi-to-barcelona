const CarService = require('../app/services/Car');
const CarRest = {};

CarRest.addCar = async (req, res) => {
    try {
        const result = await CarService.createCar(req.body);
        res.json(result);
    } catch(err) {
        console.log(err);
        res.status(500).json({message: 'Unable to add car'});
    }
};

module.exports = CarRest;
