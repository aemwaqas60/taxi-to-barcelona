const QuoteService = require('../app/services/Quote');

const QuoteRest = {};

QuoteRest.getQuote = async (req, res) => {
    try {
        const result = await QuoteService.getCarsQuote(req.query.bookingCriteriaId, req.query.carType);
        res.json(result);
    } catch(err) {
        console.log(err);
        res.status(500).json({message: 'Unable to calculate quote'});
    }
};

module.exports = QuoteRest;
