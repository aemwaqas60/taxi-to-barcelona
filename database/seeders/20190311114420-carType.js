'use strict';
const uuidv1 = require('uuid/v1');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.bulkInsert('CarType', [{
      id: uuidv1(),
      name: 'Executive Sedan',
      code: 'EXEC',
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      id: uuidv1(),
      name: 'Sedan',
      code: 'SALOON',
      createdAt: new Date(),
      updatedAt: new Date(),
    },{
      id: uuidv1(),
      name: 'MiniVan',
      code: 'MPV',
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('CarType', null, {});
  },
};
