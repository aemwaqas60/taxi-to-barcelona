'use strict';
const uuidv1 = require('uuid/v1');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.bulkInsert('PricingTable', [{
      id: uuidv1(),
      type: 'day',
      pricePerKm: 1.8,
      priceMin: 41,
      carTypeCode: 'EXEC',
      createdAt: new Date(),
      updatedAt: new Date(),
    },{
      id: uuidv1(),
      type: 'day',
      pricePerKm: '1.3',
      priceMin: 39,
      carTypeCode: 'SALOON',
      createdAt: new Date(),
      updatedAt: new Date(),
    },{
      id: uuidv1(),
      type: 'day',
      pricePerKm: 1.7,
      priceMin: 40,
      carTypeCode: 'MPV',
      createdAt: new Date(),
      updatedAt: new Date(),
    },{
      id: uuidv1(),
      type: 'night',
      pricePerKm: 1.9,
      priceMin: 45,
      carTypeCode: 'EXEC',
      createdAt: new Date(),
      updatedAt: new Date(),
    },{
      id: uuidv1(),
      type: 'night',
      pricePerKm: 1.4,
      priceMin: 40,
      carTypeCode: 'SALOON',
      createdAt: new Date(),
      updatedAt: new Date(),
    },{
      id: uuidv1(),
      type: 'night',
      pricePerKm: 1.8,
      priceMin: 41,
      carTypeCode: 'MPV',
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('PricingTable', null, {});
  },
};
