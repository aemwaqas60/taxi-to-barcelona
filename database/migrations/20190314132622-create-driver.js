'use strict';
module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('Driver', {
      name: {
        type: DataTypes.STRING
      },
      driverId: {
        allowNull: false,
        primaryKey: true,
        autoIncrement: false,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
      },
      email: {
        type: DataTypes.STRING
      },
      idCard: {
        type: DataTypes.STRING
      },
      licenseNumber: {
        type: DataTypes.STRING
      },
      insuranceNumber: {
        type: DataTypes.STRING
      },
      countryCode: {
        type: DataTypes.INTEGER
      },
      phoneNumber: {
        type: DataTypes.INTEGER
      },
      photo: {
        type: DataTypes.STRING
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
    }, {
      freezeTableName: true,
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('Driver');
  }
};