'use strict';
module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('BookingCriteria', {
      bookingCriteriaId: {
        type: DataTypes.UUID,
        primaryKey: true,
        autoIncrement: false,
        allowNull: false,
      },
      pickUpAddress: {
        type: DataTypes.STRING
      },
      pickUpId: {
        type: DataTypes.STRING
      },
      pickUpLng: {
        type: DataTypes.FLOAT
      },
      pickUpLat: {
        type: DataTypes.FLOAT
      },
      dropOffAddress: {
        type: DataTypes.STRING
      },
      dropOffId: {
        type: DataTypes.STRING
      },
      dropOffLng: {
        type: DataTypes.FLOAT
      },
      dropOffLat: {
        type: DataTypes.FLOAT
      },
      pickupTime: {
        type: DataTypes.DATE
      },
      passengerTotal: {
        type: DataTypes.INTEGER
      },
      casesTotal: {
        type: DataTypes.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      }
    }, {
      freezeTableName: true,
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('BookingCriteria');
  }
};