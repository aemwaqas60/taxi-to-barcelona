'use strict';
module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('Car', {
      id: {
        allowNull: false,
        primaryKey: true,
        autoIncrement: false,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
      },
      model: {
        type: DataTypes.STRING,
      },
      type: {
        type: DataTypes.STRING,
      },
      make: {
        type: DataTypes.INTEGER,
      },
      seats: {
        type: DataTypes.INTEGER,
      },
      bags: {
        type: DataTypes.INTEGER,
      },
      carNumber: {
        type: DataTypes.STRING,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      driverId: {
        type: DataTypes.UUID,
        references: {
          model: 'Driver',
          key: 'driverId',
        },
        onDelete: 'SET NULL',
      },
    }, {
      freezeTableName: true,
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('Car');
  }
};