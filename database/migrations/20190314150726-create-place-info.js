'use strict';
module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('PlaceInfo', {
      id: {
        allowNull: false,
        primaryKey: true,
        autoIncrement: false,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
      },
      palceId: {
        type: DataTypes.INTEGER,
      },
      info: {
        type: DataTypes.STRING,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    }, {
      freezeTableName: true,
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('PlaceInfo');
  }
};