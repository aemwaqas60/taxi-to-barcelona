'use strict';
module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('Booking', {
      bookingId: {
        allowNull: false,
        primaryKey: true,
        autoIncrement: false,
        type: DataTypes.UUID,
      },
      passengerName: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      pickupName: {
        type: DataTypes.STRING
      },
      pickupAddress: {
        type: DataTypes.STRING
      },
      pickupLng: {
        type: DataTypes.FLOAT
      },
      pickupLat: {
        type: DataTypes.FLOAT
      },
      dropoffName: {
        type: DataTypes.STRING
      },
      dropoffAddress: {
        type: DataTypes.STRING
      },
      dropoffLng: {
        type: DataTypes.FLOAT
      },
      dropoffLat: {
        type: DataTypes.FLOAT
      },
      wheelchair: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      flightNumber: {
        type: DataTypes.STRING
      },
      cost: {
        type: DataTypes.INTEGER
      },
      distanceFee: {
        type: DataTypes.INTEGER
      },
      pickupTime: {
        type: DataTypes.DATE
      },
      passengerTotal: {
        type: DataTypes.INTEGER
      },
      casesTotal: {
        type: DataTypes.INTEGER
      },
      carType: {
        type: DataTypes.STRING
      },
      notes: {
        type: DataTypes.STRING
      },
      bookingAuth: {
        type: DataTypes.STRING
      },
      bookingRef: {
        type: DataTypes.STRING
      },
      bookingStatus: {
        type: DataTypes.STRING
      },
      paymentStatus: {
        type: DataTypes.STRING
      },
      paymentId: {
        type: DataTypes.UUID
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      userId: {
        type: DataTypes.UUID,
        references: {
          model: 'User',
          key: 'userId',
        },
        onDelete: 'SET NULL',
      },
      driverId: {
        type: DataTypes.UUID,
        references: {
          model: 'Driver',
          key: 'driverId',
        },
      },
    }, {
      freezeTableName: true,
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('Booking');
  }
};