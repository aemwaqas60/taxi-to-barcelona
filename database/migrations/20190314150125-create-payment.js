'use strict';
module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('Payment', {
      id: {
        allowNull: false,
        primaryKey: true,
        autoIncrement: false,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
      },
      transactionId: {
        type: DataTypes.STRING
      },
      chargedAmount: {
        type: DataTypes.FLOAT
      },
      refunded: {
        type: DataTypes.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      bookingId: {
        type: DataTypes.UUID,
        allowNull: false,
      },
    }, {
      freezeTableName: true,
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('Payment');
  }
};