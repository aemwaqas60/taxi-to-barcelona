'use strict';

module.exports = {
    up: async (queryInterface, DataTypes) => {
        return [
            await queryInterface.addColumn(
                'BookingCriteria',
                'passengerName',
                {
                    type: DataTypes.STRING,
                    allowNull: true,
                }
            ),
            await queryInterface.addColumn(
                'BookingCriteria',
                'cost',
                {
                    type: DataTypes.INTEGER,
                    allowNull: true,
                }
            ),
            await queryInterface.addColumn(
                'BookingCriteria',
                'carType',
                {
                    type: DataTypes.STRING,
                    allowNull: true,
                }
            ),
            await queryInterface.addColumn(
                'BookingCriteria',
                'wheelChair',
                {
                    type: DataTypes.BOOLEAN,
                    defaultValue: false,
                }
            ),
            await queryInterface.addColumn(
                'BookingCriteria',
                'flightNumber',
                {
                    type: DataTypes.STRING,
                    allowNull: true,
                }
            ),
            await queryInterface.addColumn(
                'BookingCriteria',
                'bookingNote',
                {
                    type: DataTypes.STRING,
                    allowNull: true,
                }
            ),
            await queryInterface.addColumn(
                'BookingCriteria',
                'termsOfService',
                {
                    type: DataTypes.BOOLEAN,
                    defaultValue: false,
                }
            ),
        ];
    },

    down: async (queryInterface) => {
        return [
            await queryInterface.removeColumn('BookingCriteria', 'passengerName'),
            await queryInterface.removeColumn('BookingCriteria', 'cost'),
            await queryInterface.removeColumn('BookingCriteria', 'carType'),
            await queryInterface.removeColumn('BookingCriteria', 'wheelChair'),
            await queryInterface.removeColumn('BookingCriteria', 'flightNumber'),
            await queryInterface.removeColumn('BookingCriteria', 'bookingNote'),
            await queryInterface.removeColumn('BookingCriteria', 'termsOfService'),
        ];
    },
};