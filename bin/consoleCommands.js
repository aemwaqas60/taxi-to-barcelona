#!/usr/bin/env node
const program = require('commander');
const AddCarInquirer = require('./AddCar');
const AddDriverInquirer = require('./AddDriver');
const AssignDriverInquirer = require('./AssignDriver');
const ChangeDriverInquirer = require('./ChangeDriver');

/*
Commands:
Add car:        add:car
Add driver:     add:driver
Assign driver:  assign:driver
Change driver: change:driver
*/
program
    .version('0.0.1')
    .description('Taxi to Barcelona Management System');

program
    .command('addCar')
    .alias('add:car')
    .description('Add car')
    .action(async () => {
        await AddCarInquirer.addCar();
    });

program
    .command('addDriver')
    .alias('add:driver')
    .description('Add driver')
    .action(async () => {
        await AddDriverInquirer.addDriver();
    });

program
    .command('assignDriver')
    .alias('assign:driver')
    .description('Assign driver')
    .action(async () => {
        await AssignDriverInquirer.assignDriver();
    });

program
    .command('changeDriver')
    .alias('change:driver')
    .description('Change driver')
    .action(async () => {
        await ChangeDriverInquirer.changeDriver();
    });


program.parse(process.argv);
