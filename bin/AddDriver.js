const { prompt } = require('inquirer');
const DriverService = require('../app/services/Driver');
const questions = [
    {
        type : 'input',
        name : 'name',
        message : 'Name of driver: '
    },
    {
        type : 'input',
        name : 'email',
        message : 'Email: ',
    },
    {
        type : 'input',
        name : 'idCard',
        message : 'ID card number: '
    },
    {
        type : 'input',
        name : 'licenseNumber',
        message : 'License Number: '
    },
    {
        type : 'input',
        name : 'insuranceNumber',
        message : 'Insurance Number: '
    },
    {
        type : 'input',
        name : 'countryCode',
        message : 'Country Code for phone number: ',
    },
    {
        type : 'input',
        name : 'phoneNumber',
        message : 'Phone Number: ',
    },
    {
        type : 'input',
        name : 'Photo',
        message : 'Absolute path to photo: ',
    },
    {
        type: 'confirm',
        name: 'confirm',
        message: 'Are you sure you want to add the driver with the following info?: ',
    },
];

class AddDriverInquiry {

    static async addDriver(){
        prompt(questions).then(async function(driver){
            if(driver.confirm) {
                const result = await DriverService.createDriver(driver);
                if(result) {
                    console.log('Driver Created successfully');

                    return true;
                }
            } else {
                console.log('Try again to add driver');

                return false;
            }
        });
    }
}

module.exports = AddDriverInquiry;