const { prompt } = require('inquirer');
const BookingService = require('../app/services/Booking');

const questions = [
    {
        type : 'input',
        name : 'bookingId',
        message : 'Booking Id: '
    },
    {
        type : 'input',
        name : 'driverId',
        message : 'Driver Id: ',
    },
    {
        type: 'confirm',
        name: 'confirm',
        message: 'Are you sure you want to assign the driver to this booking?: ',
    },
];

class AssignDriverInquiry {

    static async assignDriver() {
        prompt(questions).then(async function(data) {
            if (data.confirm) {
                const result = await BookingService.assignDriverToBooking(data.bookingId, data.driverId);
                if (result) {
                    console.log('Successfully assigned a driver to the booking');

                    return true;
                }
            } else {
                console.log('Try again to assign driver');

                return false;
            }
        });
    }
}

module.exports = AssignDriverInquiry;
