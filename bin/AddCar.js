const { prompt } = require('inquirer');
const CarService = require('../app/services/Car');

const questions = [
        {
            type : 'input',
            name : 'model',
            message : 'Model: '
        },
        {
            type : 'list',
            name : 'type',
            message : 'Car Type: ',
            choices: ['EXEC', 'SALOON', 'MPV'],
        },
        {
            type : 'input',
            name : 'make',
            message : 'Car Make Year: '
        },
        {
            type : 'input',
            name : 'seats',
            message : 'Seats: '
        },
        {
            type : 'input',
            name : 'bags',
            message : 'Bags: '
        },
        {
            type : 'input',
            name : 'carNumber',
            message : 'Car Number: ',
        },
        {
            type : 'input',
            name : 'driverId',
            message : 'Driver ID: ',
        },
        {
            type: 'confirm',
            name: 'confirm',
            message: 'Are you sure you want to add the car with the following info?: ',
        },
    ];

class AddCarInquiry {

    static async addCar() {
        prompt(questions).then(async function(car){
            if(car.confirm) {
                const result = await CarService.createCar(car);
                if(result) {
                    console.log('Car Created successfully');

                    return true;
                }
            } else {
                console.log('Try again to add Car');
                return false;
            }
        });
    }
}

module.exports = AddCarInquiry;