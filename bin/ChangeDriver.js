const { prompt } = require('inquirer');
const BookingService = require('../app/services/Booking');

const questions = [
    {
        type : 'input',
        name : 'bookingId',
        message : 'Booking Id: '
    },
    {
        type : 'input',
        name : 'driverId',
        message : 'Driver Id: ',
    },
    {
        type: 'confirm',
        name: 'confirm',
        message: 'Are you sure you want to change the driver for this booking?: ',
    },
];

class ChangeDriverInquiry {

    static async changeDriver() {
        prompt(questions).then(async function(data) {
            if (data.confirm) {
                const result = await BookingService.assignDriverToBooking(data.bookingId, data.driverId);
                if (result) {
                    console.log('Successfully changed the driver');

                    return true;
                }
            } else {
                console.log('Try again to change the driver');

                return false;
            }
        });
    }
}

module.exports = ChangeDriverInquiry;