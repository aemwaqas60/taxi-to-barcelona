require('dotenv').config();

const env = process.env.NODE_ENV;
const SQLdb = {
};

SQLdb[env] = {
    username: process.env.SQL_DB_USERNAME,
    password: process.env.SQL_DB_PASSWORD,
    database: process.env.SQL_DB_DATABASE,
    host: process.env.SQL_DB_HOST,
    dialect: process.env.SQL_DB_DIALECT,
    port: process.env.SQL_DB_PORT,
    logging: process.env.SQL_LOG_STATUS || false,
    seederStorage: process.env.SQL_SEED_STORAGE || 'sequelize',
    operatorsAliases: false,
};

module.exports = SQLdb;