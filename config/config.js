require('dotenv').config();

module.exports = {
    env: process.env.NODE_ENV,
    port: process.env.PORT || 3000,
    filePath: process.env.FILE_PATH,
    baseURL: process.env.BASE_URL,
    SQLdb: {
        username: process.env.SQL_DB_USERNAME,
        password: process.env.SQL_DB_PASSWORD,
        database: process.env.SQL_DB_DATABASE,
        host: process.env.SQL_DB_HOST,
        dialect: process.env.SQL_DB_DIALECT,
        port: process.env.SQL_DB_PORT,
        logging: process.env.SQL_LOG_STATUS || false,
        seederStorage: process.env.SQL_SEED_STORAGE || 'sequelize',
        operatorsAliases: false,
    },
    alertEmail: {
        host: process.env.MAIL_HOST,
        from: process.env.MAIL_ALERT_FROM,
        port: process.env.MAIL_PORT,
        auth: {
            user: process.env.MAIL_USERNAME,
            pass: process.env.MAIL_PASSWORD,
        },
        bcc: ['mmesunny@gmail.com','support@taxitobarcelona.com', 'nabeelabbas36@yahoo.com'],
    },
    jwtSecret: process.env.JWT_SECRET,
    stripe: {
        key: process.env.STRIPE_KEY,
        secret: process.env.STRIPE_SECRET,
    },
    facebookAuth : {
        clientID: process.env.FACEBOOK_CLIENT_ID,
        clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
        callbackURL: process.env.FACEBOOK_CALLBACK_URL,
        profileFields: ['id', 'email', 'name'] // For requesting permissions from Facebook API
    },
    googleAuth : {
        clientID: process.env.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_CLIENT_SECRET,
        callbackURL: process.env.GOOGLE_CALLBACK_URL,
        profileFields: ['id', 'email', 'name'] // For requesting permissions from Facebook API
    },
    googleMapApiKey: process.env.GOOGLE_API_KEY,
    appBaseURL : process.env.APP_BASE_URL,
    bookingQuote: {
        minDistanceKM: 15,
        nightHoursStart: 18,
        nightHoursEnd: 7,
    },
    S3: {
        driver: process.env.S3_DRIVER,
        key: process.env.S3_KEY,
        secret: process.env.S3_SECRET,
        region: process.env.S3_REGION,
        bucket: process.env.S3_BUCKET,
        url: process.env.S3_URL,
    },
};
