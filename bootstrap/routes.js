const app = require('./expressApp');
const userRoutes = require('../routes/user');
const adminRoutes = require('../routes/admin');
const bookingRoutes = require('../routes/booking');
const quoteRoutes = require('../routes/quote');
const bookingCriteriaRoutes = require('../routes/bookingCriteria');

app.use('/api/v1', userRoutes);
app.use('/api/v1', bookingRoutes);
app.use('/api/v1', quoteRoutes);
app.use('/api/v1', bookingCriteriaRoutes);
app.use('/api/v1/admin', adminRoutes);

module.exports = app;
